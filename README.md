CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
-----------
This module helps you to exclude particular taxonomy page

Uses:

1. This module makes easier to use by just checking the exclude from apache Solr 
checkbox option.

2. Helpful to exclude the taxonomy page from search box. 

Note: IMPORTANT.!! You need to re-index Apache solr search index 
to see the new changes. 

REQUIREMENTS
------------
- Apache Solr Search Integration (http://drupal.org/project/apachesolr)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Does not require any configuration. Each taxonomy will have an checkbox 
 option to choose the term page to exclude.

TROUBLESHOOTING
---------------

MAINTAINERS
-----------
Current maintainers:
Aaron23 https://www.drupal.org/u/aaron23
